import { Component, OnInit } from '@angular/core';
import {employee_details} from '../employee/employee-details';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {
  employeeDetails : any[] = employee_details;
 
  constructor() { }

  ngOnInit(): void {}

    getPhone(value) {
      if (value && !isNaN(value)) {
        return value;
      } else {
        return 'NA';
      }
    }

}
